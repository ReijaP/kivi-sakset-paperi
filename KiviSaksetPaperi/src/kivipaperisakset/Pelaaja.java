
package kivipaperisakset;

/*
 * @author Reija Parvio
 */

public class Pelaaja {
	private String valinta = "";
	
	/*
	 * Metodi arpoo luvun 0 ja 2 välistä,
	 * ja tallentaa valinta-muuttujaan merkkijono-arvon,
	 * ja palauttaa valinnan merkkijonona
	 */
    public void valitse() {
    	
        int c = (int) (Math.random() * 3);
        switch (c) {
            case 0:
                setValinta("kivi");
                break;
            case 1:
                setValinta("paperi");
                break;
            case 2:
                setValinta("sakset");
                break;
        }
        }
    
    public void setValinta(String valinta) {
    	this.valinta = valinta;
    }
    
    public String getValinta() {
    	return this.valinta;
    }
}
