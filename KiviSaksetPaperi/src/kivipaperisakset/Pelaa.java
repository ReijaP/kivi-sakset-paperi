package kivipaperisakset;

/*
 * @author Reija Parvio
 */
public class Pelaa {
	
	//Luodaan pelaaja-oliot
	private Pelaaja pelaaja1;
	private Pelaaja pelaaja2;
	
	// Alustetaan pisteidenlasku-olio
	private Pisteidenlasku pisteet;

	private int tasapelit = 0;

	/*
	 * Konstruktorissa tallennetaan saadut pelaajat muuttujiin.
	 * Luodaan pisteidenlasku-olio, jolle annetaan pelaajat parametreinä.
	 */
	public Pelaa(Pelaaja pl1, Pelaaja pl2) {
		this.pelaaja1 = pl1;
		this.pelaaja2 = pl2;
		this.pisteet = new Pisteidenlasku(pelaaja1, pelaaja2);
	}

	/*
	 * Metodi hoitaa pelin kulun ja tulostaa konsoliin tilanteen 
	 */
	public void pelaaPeliä() {
		int pelatuterät = 0;
		String p1Valinta;
		String p2Valinta;

		// Toistetaan joka kierroksella, kunnes peliLoppuu-metodi palauttaa true
		do {
			System.out.println("\n====== Erä: " + pelatuterät + " ======\n");
			
			pelaaja1.valitse();
			pelaaja2.valitse();
			p1Valinta = pelaaja1.getValinta();
			p2Valinta = pelaaja2.getValinta();
			
			System.out.println("Pelaaja 1: " + p1Valinta);
			System.out.println("Pelaaja 2: " + p2Valinta);
			
			// Annetaan pelaajien valinnat metodille kumpiVoittaa, joka määrittää voittajan
			System.out.println(kumpiVoittaa(p1Valinta, p2Valinta));
			
			pelatuterät++;

			System.out.println("Pelaajalla 1 koossa " + this.pisteet.getPisteet(pelaaja1) + " pistettä.");
			System.out.println("Pelaajalla 2 koossa " + this.pisteet.getPisteet(pelaaja2) + " pistettä.");

			System.out.println("Tasapelien lukumäärä: " + tasapelit + "\n");

		} while (!peliLoppuu(pisteet));
	}

	/*
	 * Metodi hakee molempien pelaajien tämänhetkisen pistetilanteen.
	 * Jos jommalla kummalla pelaajalla on kasassa 3 pistettä, metodi palauttaa true,
	 * muuten metodi palauttaa false, ja peli jatkuu
	 */
	public boolean peliLoppuu(Pisteidenlasku pisteet) {
		if ((pisteet.getPisteet(pelaaja1) >= 3) || (pisteet.getPisteet(pelaaja2) >= 3)) {
			System.out.println("KOLME VOITTOA - PELI PÄÄTTYY");
			return true;
		}
		return false;
	}

	/*
	 * Metodi saa parametreinä pelaajien valinnat.
	 * Metodi vertailee pelaajien valintoja, kasvattaa voittajan pisteitä yhdellä,
	 * ja palauttaa voittajan 
	 */
	public String kumpiVoittaa(String p1Valinta, String p2Valinta) {
		String voittoilmoitus = "Pelaaja 1 voittaa!";

		if (p1Valinta == p2Valinta) {
			tasapelit++;
			return "Tasapeli!";
		} else {

			if ((p1Valinta.equals("paperi")) && (p2Valinta.equals("kivi"))) {
				this.pisteet.annaPiste(pelaaja1);
				return voittoilmoitus;
			} else if ((p1Valinta.equals("kivi")) && (p2Valinta.equals("sakset"))) {
				this.pisteet.annaPiste(pelaaja1);
				return voittoilmoitus;
			} else if ((p1Valinta.equals("sakset")) && (p2Valinta.equals("paperi"))) {
				this.pisteet.annaPiste(pelaaja1);
				return voittoilmoitus;
			} else {
				this.pisteet.annaPiste(pelaaja2);
				return "Pelaaja 2 voittaa!";
			}
		}
	}
}
