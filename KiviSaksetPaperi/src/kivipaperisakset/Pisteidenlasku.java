package kivipaperisakset;

import java.util.HashMap;

/*
 * @author Reija Parvio
 */
public class Pisteidenlasku {
	
	// Tallennetaan pelaajan pisteet hashMap:iin, jossa avaimena toimii pelaajan nimi 
	private HashMap<Pelaaja, Integer> pelaajienPisteet = new HashMap<Pelaaja, Integer>();
	
	/*
	 * Konstruktori lisää parametreinä saadut pelaajat hashMap:iin
	 */
	public Pisteidenlasku(Pelaaja pelaaja1, Pelaaja pelaaja2) {
		this.pelaajienPisteet.put(pelaaja1, 0);
		this.pelaajienPisteet.put(pelaaja2, 0);
	}
	
	/*
	 * Metodi kasvattaa parametrinä saadun avaimen arvoa yhdellä
	 */
	public void annaPiste(Pelaaja pelaaja) {
		this.pelaajienPisteet.replace(pelaaja, pelaajienPisteet.get(pelaaja) + 1);
	}
	
	/*
	 * Metodi palauttaa parametrinä saadun avaimen arvon
	 */
	public int getPisteet(Pelaaja pelaaja) {
		return this.pelaajienPisteet.get(pelaaja);
	}

}
