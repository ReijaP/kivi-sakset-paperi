package kivipaperisakset;

public class Peli {
	
/*
 * @author Reija Parvio
 */
	public static void main(String[] args) {
		
		// Luodaan tarvittava määrä pelaajia
		Pelaaja pelaaja1 = new Pelaaja();
		Pelaaja pelaaja2 = new Pelaaja();
		
		// Luodaan peli, jolle annetaan pelaajat parametreinä
		Pelaa peli = new Pelaa(pelaaja1, pelaaja2);
		
		// Käynnistetään peli
		peli.pelaaPeliä();

	}

}
