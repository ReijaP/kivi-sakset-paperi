package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import kivipaperisakset.Pelaaja;

class PelaajaTest {
	
	private static Pelaaja pelaaja;
	
	@BeforeAll
    public static void testluoPelaaja() {
		pelaaja = new Pelaaja();
	}
	
	@ParameterizedTest (name="Valinnan {0} asetus palauttaa valinnan {1}")
	@CsvSource({ "kivi, kivi", "paperi, paperi", "sakset, sakset"})
	public void testSetValinta(String valinta, String tulos) {
		pelaaja.setValinta(valinta);
		assertEquals(tulos, pelaaja.getValinta(), "Valinta tallentui väärin");
	}
	
	@Test
	public void testValitse() {
		pelaaja.valitse();
		if (pelaaja.getValinta() == "kivi")
		assertEquals("kivi", pelaaja.getValinta(), "Valinta tallentui väärin");
		else if (pelaaja.getValinta() == "paperi")
			assertEquals("paperi", pelaaja.getValinta(), "Valinta tallentui väärin");
		else if (pelaaja.getValinta() == "sakset")
			assertEquals("sakset", pelaaja.getValinta(), "Valinta tallentui väärin");
	}
}
