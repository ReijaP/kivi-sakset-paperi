package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import kivipaperisakset.Pelaaja;
import kivipaperisakset.Pisteidenlasku;

class PisteidenlaskuTest {
	
	private static Pelaaja pelaaja1;
	private static Pelaaja pelaaja2;
	private static Pisteidenlasku pl;
	
	@BeforeAll
    public static void testluoPelaaja() {
		pelaaja1 = new Pelaaja();
		pelaaja2 = new Pelaaja();
		pl = new Pisteidenlasku(pelaaja1, pelaaja2);
	}


	@Test
	void testAnnaPisteet1() {
		pl.annaPiste(pelaaja1);
		assertEquals(1, pl.getPisteet(pelaaja1), "Pelaajalla 1 väärä pistemäärä");
	}
	
	@Test
	void testAnnaPisteet2() {
		pl.annaPiste(pelaaja2);
		assertEquals(1, pl.getPisteet(pelaaja2), "Pelaajalla 2 väärä pistemäärä");
	}

}
