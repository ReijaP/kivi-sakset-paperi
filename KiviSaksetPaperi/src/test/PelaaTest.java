package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import kivipaperisakset.Pelaa;
import kivipaperisakset.Pelaaja;
import kivipaperisakset.Pisteidenlasku;

class PelaaTest {

	private Pelaaja pelaaja1;
	private Pelaaja pelaaja2;
	private Pisteidenlasku pisteet;
	private Pelaa pelaa;

	@BeforeEach
	public void luoPelaajat() {
		pelaaja1 = new Pelaaja();
		pelaaja2 = new Pelaaja();
		pisteet = new Pisteidenlasku(pelaaja1, pelaaja2);
		pelaa = new Pelaa(pelaaja1, pelaaja2);
	}
	
	@Test
	void testPeliLoppuu2() {
		pisteet.annaPiste(pelaaja1);
		assertEquals(false, pelaa.peliLoppuu(pisteet), "Palautuksen tuli olla false");
	}

	@Test
	void testPeliLoppuu1() {

		pisteet.annaPiste(pelaaja1);
		pisteet.annaPiste(pelaaja1);
		pisteet.annaPiste(pelaaja1);
		pisteet.annaPiste(pelaaja1);
		assertEquals(true, pelaa.peliLoppuu(pisteet), "Palautuksen tuli olla true");
	}

	
	@ParameterizedTest (name="Valinta {0} voittaa valinnan {1} saaden ilmoituksen {2}")
	@CsvSource({ "kivi, paperi, Pelaaja 2 voittaa!", "paperi, kivi, Pelaaja 1 voittaa!", "paperi, sakset, Pelaaja 2 voittaa!", 
		"sakset, paperi, Pelaaja 1 voittaa!", "sakset, kivi, Pelaaja 2 voittaa!", "kivi, sakset, Pelaaja 1 voittaa!"})
	public void testKumpiVoittaa2(String valinta1, String valinta2, String tulos) {
		assertEquals(tulos, pelaa.kumpiVoittaa(valinta1, valinta2), "Väärä pelaaja voitti");
	}
	
	@Test
	void testTasapeli() {
		assertEquals("Tasapeli!", pelaa.kumpiVoittaa("kivi", "kivi"), "Piti olla tasapeli");
	}

}
